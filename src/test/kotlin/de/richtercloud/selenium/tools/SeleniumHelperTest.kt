/**
 * Copyright 2018-2021 Karl-Philipp Richter
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package de.richtercloud.selenium.tools

import com.natpryce.hamkrest.Matcher
import com.natpryce.hamkrest.nothing
import de.richtercloud.selenium.tools.SeleniumHelper.Companion.randomString
import org.openqa.selenium.support.ui.FluentWait
import org.openqa.selenium.support.ui.WebDriverWait
import java.io.File
import java.io.IOException
import java.lang.ExceptionInInitializerError
import java.lang.IllegalArgumentException
import org.openqa.selenium.WebDriver
import de.richtercloud.jsf.validation.service.ValidatorMessageSeverity
import de.richtercloud.jsf.validation.service.ValidatorMessage
import net.jqwik.api.constraints.WithNull
import org.openqa.selenium.WebElement
import org.openqa.selenium.remote.RemoteWebDriver
import org.apache.commons.lang3.RandomStringUtils
import de.richtercloud.selenium.tools.SeleniumHelper.TextToBePresentInElementWithText
import de.richtercloud.jsf.validation.service.ValidationService
import io.mockk.*
import net.jqwik.api.ForAll
import net.jqwik.api.Property
import net.jqwik.api.constraints.Size
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.openqa.selenium.OutputType
import org.openqa.selenium.TimeoutException
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.InputStream
import java.nio.file.Files
import java.util.*
import java.util.concurrent.TimeUnit
import java.util.function.Function

class SeleniumHelperTest {
    companion object {
        private val LOGGER = LoggerFactory.getLogger(SeleniumHelperTest::class.java)
        private val RANDOM: Random = createRandom()

        private fun createRandom(): Random {
            val randomSeed = System.currentTimeMillis()
            LOGGER.debug("randomSeed: $randomSeed")
            return Random(randomSeed)
        }

        private val SCREENSHOT_DIR: File = createScreenshotDir()

        private fun createScreenshotDir(): File =
            try {
                Files.createTempDirectory(SeleniumHelperTest::class.java.simpleName).toFile()
            } catch (ex: IOException) {
                throw ExceptionInInitializerError(ex)
            }
    }

    @Test
    fun testRandomStringZero() {
        Assertions.assertThrows(IllegalArgumentException::class.java) { randomString(0) }
    }

    @Test
    fun testRandomSizeRandom() {
        val length = RANDOM.nextInt(1024)
        val result = randomString(length)
        Assertions.assertEquals(
            length,
            result.length,
            "length mismatch"
        )
    }

    @Test
    fun testLogPageSource() {
        LOGGER.info("logPageSource")
        val browser = mockk<WebDriver>(relaxed = true)
        val logger = mockk<Logger>(relaxed = true)
        val instance = SeleniumHelper()
        every { browser.pageSource } returns "<!DOCTYPE html><html><head><title>title</title></head><body><div>a</div></body></html>"
        instance.logPageSource(
            browser,
            logger
        )
        verify { logger.info("browser page source:\n===\n<!DOCTYPE html><html><head><title>title</title></head><body><div>a</div></body></html>\n===") }
    }

    @Test
    fun testLogPageSourcePretty() {
        LOGGER.info("logPageSource")
        val browser = mockk<WebDriver>(relaxed = true)
        val logger = mockk<Logger>(relaxed = true)
        val instance = SeleniumHelper()
        every { browser.pageSource } returns "<!DOCTYPE html><html lang=\"en\"><head><title>title</title></head><body><div>a</div></body></html>"
        instance.logPageSourcePretty(
            browser,
            logger,
            ValidatorMessageSeverity.MINIMUM,
            nothing
        )
        verify { logger.info("browser page source:\n===\n<!doctype html>\n<html lang=\"en\">\n <head>\n  <title>title</title>\n </head>\n <body>\n  <div>\n   a\n  </div>\n </body>\n</html>\n===") }
        //@TODO: `<head/>` should be `<head></head>` since no manipuation
        //except whitespace ought to take place, asked
        //https://stackoverflow.com/questions/49555441/how-to-avoid-html-being-manipulated-during-pretty-printing-with-lsserializer
        //for input
    }

    @Property
    fun testAssertMessagesContainsAny(
        @ForAll texts: @Size(min = 0, max = 10) MutableSet<String>,
        @ForAll messagesContainsAnyText: Boolean
    ) {
        LOGGER.info("assertMessagesContainsAny")
        LOGGER.trace("testAssertMessagesContainsAny texts: $texts")
        LOGGER.trace("testAssertMessagesContainsAny messagesContainsAnyText: $messagesContainsAnyText")
        val instance = SeleniumHelper(
            Files.createTempDirectory(
                SCREENSHOT_DIR.toPath(),
                "testAssertMessagesContainsAny"
            ).toFile()
        )
        if (texts.isEmpty()) {
            val browser = mockk<WebDriver>(relaxed = true)
            val messages = mockk<WebElement>(relaxed = true)
            Assertions.assertThrows(
                IllegalArgumentException::class.java
            ) {
                instance.assertMessagesContainsAny(
                    browser,
                    messages,
                    texts
                )
            }
            return
        }
        if (texts.contains(null)) {
            val browser = mockk<WebDriver>(relaxed = true)
            val messages = mockk<WebElement>(relaxed = true)
            Assertions.assertThrows(
                IllegalArgumentException::class.java
            ) {
                instance.assertMessagesContainsAny(
                    browser,
                    messages,
                    texts
                )
            }
            return
        }
        val browser = mockk<RemoteWebDriver>(relaxed = true)
        //needs to be RemoteWebDriver in order to be able to cast to
        //TakesScreenshot
        val messages = mockk<WebElement>(relaxed = true)
        val messagesText: String?
        if (messagesContainsAnyText) {
            val textsRandomIndex = RANDOM.nextInt(texts.size)
            LOGGER.trace("testAssertMessagesContainsAny textsRandomIndex: $textsRandomIndex")
            messagesText = ArrayList(texts)[textsRandomIndex]
        } else {
            var textNotInTexts: String?
            do {
                textNotInTexts = RandomStringUtils.random(RANDOM.nextInt(20))
            } while (texts.contains(textNotInTexts))
            messagesText = textNotInTexts
        }
        LOGGER.trace("testAssertMessagesContainsAny messagesChildText: $messagesText")
        every { messages.text } returns messagesText
        //the following constructor mocks have no effect in JUnit 5 (reported at
        //https://github.com/powermock/powermock/issues/885 and covered by
        //https://stackoverflow.com/questions/48794259/powermock-mockito-mocking-the-constructor-does-not-work/48807773#48807773
        //), so use the created factory methods in SeleniumHelper which can be
        //removed as soon as the support is fixed (jqwik only works with
        //JUnit 5, so downgrading is not an option)
        val fluentWait: FluentWait<WebDriver> = mockk(relaxed = true)
        val webDriverWait = mockk<WebDriverWait>(relaxed = true)
        mockkConstructor(FluentWait::class)
        mockkConstructor(WebDriverWait::class)
        every { constructedWith<FluentWait<WebDriver>>(OfTypeMatcher<WebDriver>(WebDriver::class)).withTimeout(any(), any()) } returns fluentWait
        every { constructedWith<FluentWait<WebDriver>>(OfTypeMatcher<WebDriver>(WebDriver::class)).pollingEvery(any(), any()) } returns fluentWait
        every { constructedWith<FluentWait<WebDriver>>(OfTypeMatcher<WebDriver>(WebDriver::class)).ignoring(any()) } returns fluentWait
        val slot = slot<Function<WebDriver, Any>>()
        every { constructedWith<WebDriverWait>(OfTypeMatcher<WebDriver>(WebDriver::class), OfTypeMatcher<Long>(Long::class)).until(capture(slot)) } answers {
            LOGGER.trace(
                "testAssertMessagesContainsAny answer invocation.args[0]: ${invocation.args[0]}"
            )
            if (invocation.args[0] is TextToBePresentInElementWithText) {
                val textToBePresent = invocation.args[0] as TextToBePresentInElementWithText
                LOGGER.trace("testAssertMessagesContainsAny answer  textToBePresent.text: ${textToBePresent.text}")
                if (textToBePresent.text != messagesText) {
                    throw TimeoutException()
                }
            }
        }
        every { browser.getScreenshotAs(OutputType.FILE) } returns Files.createTempFile(
            SCREENSHOT_DIR.toPath(),
            SeleniumHelperTest::class.java.simpleName,  //prefix
            "screenshot-stub" //suffix
        ).toFile()
        //creating a real file avoids the need to mock static FileUtils.copy
        //or File
        val instance1: SeleniumHelper = object : SeleniumHelper(
            Files.createTempDirectory(
                SCREENSHOT_DIR.toPath(),
                "testAssertMessagesContainsAny"
            ).toFile()
        ) {
            override fun createFluentWait(webDriver: WebDriver): FluentWait<WebDriver> {
                return fluentWait
            }

            override fun createWebDriverWait(webDriver: WebDriver, timeOutInSeconds: Long): WebDriverWait {
                return webDriverWait
            }
        }
        if (!messagesContainsAnyText) {
            Assertions.assertThrows(
                MessageTextNotContainedException::class.java
            ) {
                instance1.assertMessagesContainsAny(
                    browser,
                    messages,
                    texts
                )
            }
        } else {
            instance1.assertMessagesContainsAny(
                browser,
                messages,
                texts
            )
        }
    }

    @Test
    fun testAssertMessagesContainsAnySingle() {
        LOGGER.info("assertMessagesContainsAnySingle")
        testAssertMessagesContainsAny(
            HashSet(Arrays.asList("", "FA5\nAc37lO5ROnvTnEogVQiL1\n")),
            false
        )
    }

    @Test
    fun testValidatePageSourceIgnoreDefaultNoMessages() {
        val validationService = mockk<ValidationService>(relaxed = true)
        val instance = SeleniumHelper(
            SeleniumHelper.SCREENSHOT_START_MARKER_DEFAULT,
            SeleniumHelper.SCREENSHOT_END_MARKER_DEFAULT,
            SeleniumHelper.PAGE_SOURCE_START_MARKER_DEFAULT,
            SeleniumHelper.PAGE_SOURCE_END_MARKER_DEFAULT,
            SeleniumHelper.WEB_DRIVER_WAIT_TIMEOUT_DEFAULT,
            Files.createTempDirectory(SeleniumHelperTest::class.java.simpleName).toFile(),
            validationService
        )
        instance.validatePageSource("page source", ValidatorMessageSeverity.WARNING)
    }

    @Test
    fun testValidatePageSourceIgnoreDefault() {
        val validationService = mockk<ValidationService>()
        val instance = SeleniumHelper(
            SeleniumHelper.SCREENSHOT_START_MARKER_DEFAULT,
            SeleniumHelper.SCREENSHOT_END_MARKER_DEFAULT,
            SeleniumHelper.PAGE_SOURCE_START_MARKER_DEFAULT,
            SeleniumHelper.PAGE_SOURCE_END_MARKER_DEFAULT,
            SeleniumHelper.WEB_DRIVER_WAIT_TIMEOUT_DEFAULT,
            Files.createTempDirectory(SeleniumHelperTest::class.java.simpleName).toFile(),
            validationService
        )
        val validatorMessage = ValidatorMessage(ValidatorMessageSeverity.WARNING, 0, 0, 0, message = null, extract = null, 0, 0)
        every { validationService.validateMessages(any<InputStream>(), any<ValidatorMessageSeverity>(), any<Matcher<Any>>())} returns listOf(validatorMessage)
        Assertions.assertThrows(
            PageSourceInvalidException::class.java
        ) { instance.validatePageSource("page source", ValidatorMessageSeverity.WARNING) }
    }
}