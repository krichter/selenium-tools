/**
 * Copyright 2018-2021 Karl-Philipp Richter
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package de.richtercloud.selenium.tools

import com.natpryce.hamkrest.Matcher
import com.natpryce.hamkrest.equalTo
import de.richtercloud.selenium.tools.SeleniumAssertionHelper.assertThat
import de.richtercloud.selenium.tools.SeleniumAssertionHelper.assertEqualsHtml
import de.richtercloud.selenium.tools.SeleniumAssertionHelper.equalToHtml
import org.openqa.selenium.WebDriver
import java.lang.IllegalArgumentException
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import java.lang.AssertionError

internal class SeleniumAssertionHelperTest {

    @Test
    fun assertThatWaitExceptionActionsEmptyIllegalArgumentException() {
        val condition = mockk<Matcher<Boolean>>()
        val seleniumHelper = mockk<SeleniumHelper>()
        val webDriver = mockk<WebDriver>()
        val description = ""
        val thrownException = Assertions.assertThrows(
            IllegalArgumentException::class.java
        ) { assertThat(false, condition, seleniumHelper, webDriver, description) }
        Assertions.assertEquals("waitExceptionActions mustn't be empty", thrownException.message)
    }

    @Test
    fun assertThatGivenNoMatchThenActionExecuted() {
        val condition: Matcher<Boolean> = equalTo(false)
        val seleniumHelper = mockk<SeleniumHelper>(relaxed = true)
        val webDriver = mockk<WebDriver>(relaxed = true)
        val description = ""
        val waitExceptionAction = mockk<WaitExceptionAction>(relaxed = true)
        Assertions.assertThrows(
            AssertionError::class.java
        ) { assertThat(true, condition, seleniumHelper, webDriver, description, waitExceptionAction) }
        verify { waitExceptionAction.perform(seleniumHelper, webDriver, description) }
    }

    @Test
    fun assertThatGivenMatchThenActionNotExecuted() {
        val condition: Matcher<Boolean> = equalTo(true)
        val seleniumHelper = mockk<SeleniumHelper>(relaxed = true)
        val webDriver = mockk<WebDriver>(relaxed = true)
        val description = ""
        val waitExceptionAction = mockk<WaitExceptionAction>(relaxed = true)
        assertThat(true, condition, seleniumHelper, webDriver, description, waitExceptionAction)
        verify(atLeast = 0, atMost = 0) { waitExceptionAction.perform(any(), any(), any()) }
    }

    @Test
    fun assertEqualsHtmlSpace() {
        assertEqualsHtml("abc def", "abc def")
    }

    @Test
    fun assertEqualsHtmlDoubleSpace() {
        assertEqualsHtml("abc  def", "abc def")
    }

    @Test
    fun assertEqualsHtmlNoSpaces() {
        assertEqualsHtml("abcdef", "abcdef")
    }

    @Test
    fun equalToHtmlSpace() {
        com.natpryce.hamkrest.assertion.assertThat("123 def", equalToHtml("123 def"))
    }

    @Test
    fun equalToHtmlDoubleSpace() {
        com.natpryce.hamkrest.assertion.assertThat("123  def", equalToHtml("123 def"))
    }

    @Test
    fun equalToHtmlNoSpaces() {
        com.natpryce.hamkrest.assertion.assertThat("123def", equalToHtml("123def"))
    }
}