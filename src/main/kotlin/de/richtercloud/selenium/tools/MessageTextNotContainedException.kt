/**
 * Copyright 2018-2021 Karl-Philipp Richter
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package de.richtercloud.selenium.tools

import java.lang.RuntimeException

class MessageTextNotContainedException : RuntimeException {
    constructor(
        messageText: String?,
        messagesTextContent: String?
    ) : super(
        "The message text '$messageText' is not contained in the "
                + "messages element (has textContent '$messagesTextContent')"
    )

    constructor(
        messageTexts: Set<String?>?,
        messagesTextContext: String?
    ) : super(
        "None of the specified texts '$messageTexts' were "
                + "contained in the messages elements (has textContent '$messagesTextContext')"
    )

    companion object {
        private const val serialVersionUID = 1L
    }
}