/**
 * Copyright 2018-2021 Karl-Philipp Richter
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package de.richtercloud.selenium.tools

import com.natpryce.hamkrest.MatchResult
import com.natpryce.hamkrest.Matcher
import kotlin.Throws
import org.openqa.selenium.WebDriver
import java.lang.AssertionError
import org.junit.jupiter.api.Assertions

object SeleniumAssertionHelper {

    @JvmStatic
    @Throws(WebDriverWaitException::class)
    fun <T> assertThat(
        actual: T,
        condition: Matcher<T>,
        seleniumHelper: SeleniumHelper,
        webDriver: WebDriver,
        description: String,
        vararg waitExceptionActions: WaitExceptionAction
    ) {
        require(waitExceptionActions.isNotEmpty()) { "waitExceptionActions mustn't be empty" }
        try {
            com.natpryce.hamkrest.assertion.assertThat(actual, condition, { "" })
        } catch (err: AssertionError) {
            for (waitExceptionAction in waitExceptionActions) {
                waitExceptionAction.perform(seleniumHelper, webDriver, description)
            }
            throw err
        }
    }

    @JvmStatic
    fun assertEqualsHtml(expected: String, actual: String) {
        Assertions.assertEquals(
            expected.replace("[ ]+".toRegex(), " "),
            actual.replace("[ ]+".toRegex(), " ")
        )
    }

    @JvmStatic
    fun equalToHtml(expected: String?): Matcher<String> {
        return IsEqualHtml(expected)
    }

    private class IsEqualHtml(private val expectedValue: String?) : Matcher<String?> {
        override fun invoke(actualValue: String?): MatchResult {
            if (expectedValue == null && actualValue == null) {
                return MatchResult.Match
            }
            if (actualValue !is String) {
                return MatchResult.Mismatch("actual value is not a string, but ${actualValue?.javaClass!!.name}")
            }
            return areEqual(actualValue, expectedValue)
        }

        override val description: String = "expectedValue: $expectedValue"

        companion object {
            private fun areEqual(actual: String?, expected: String?): MatchResult {
                return if (expected != null && actual != null) {
                    if (actual.replace("[ ]+".toRegex(), " ") == expected) {
                        MatchResult.Match
                    } else {
                        MatchResult.Mismatch("")
                    }
                } else if (expected == null && actual == null) {
                    MatchResult.Match
                } else {
                    MatchResult.Mismatch("")
                }
            }
        }
    }
}