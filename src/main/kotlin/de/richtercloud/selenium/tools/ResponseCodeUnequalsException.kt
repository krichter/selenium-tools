/**
 * Copyright 2018-2021 Karl-Philipp Richter
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package de.richtercloud.selenium.tools

import org.apache.commons.io.IOUtils
import org.apache.http.HttpResponse
import java.lang.RuntimeException
import java.net.HttpURLConnection
import java.nio.charset.StandardCharsets

class ResponseCodeUnequalsException : RuntimeException {

    constructor(uRLConnection: HttpURLConnection) : super(
        "response code was ${uRLConnection.responseCode} and response text '${
            retrieveResponseTextConnection(
                uRLConnection
            )
        }'"
    )

    constructor(httpResponse: HttpResponse) : super(
        "response code was ${httpResponse.statusLine.statusCode} and response text '${
            retrieveResponseTextResponse(
                httpResponse
            )
        }'"
    )

    companion object {
        private const val serialVersionUID = 1L

        private fun retrieveResponseTextConnection(uRLConnection: HttpURLConnection): String =
            IOUtils.toString(
                if (uRLConnection.errorStream != null) uRLConnection.errorStream else uRLConnection.inputStream,
                StandardCharsets.UTF_8
            )

        private fun retrieveResponseTextResponse(httpResponse: HttpResponse): String =
            IOUtils.toString(
                httpResponse.entity.content,
                StandardCharsets.UTF_8
            )
    }
}