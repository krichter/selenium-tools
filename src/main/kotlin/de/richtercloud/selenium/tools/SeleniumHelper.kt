/**
 * Copyright 2018-2021 Karl-Philipp Richter
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package de.richtercloud.selenium.tools

import com.natpryce.hamkrest.Matcher
import com.natpryce.hamkrest.nothing
import kotlin.jvm.JvmOverloads
import java.io.File
import de.richtercloud.jsf.validation.service.ValidationService
import de.richtercloud.jsf.validation.service.MemoryValidationService
import de.richtercloud.selenium.tools.SeleniumHelper
import kotlin.Throws
import de.richtercloud.selenium.tools.WebDriverWaitException
import org.openqa.selenium.support.ui.ExpectedConditions
import de.richtercloud.selenium.tools.MessageTextNotContainedException
import org.openqa.selenium.support.ui.ExpectedCondition
import java.lang.IllegalArgumentException
import java.io.IOException
import java.util.zip.GZIPOutputStream
import java.io.ByteArrayInputStream
import org.apache.commons.io.FileUtils
import ru.yandex.qatools.ashot.Screenshot
import ru.yandex.qatools.ashot.AShot
import ru.yandex.qatools.ashot.shooting.ShootingStrategies
import javax.imageio.ImageIO
import de.richtercloud.selenium.tools.PageSourceInvalidException
import de.richtercloud.jsf.validation.service.ValidatorMessageSeverity
import de.richtercloud.jsf.validation.service.ValidatorMessage
import javax.xml.parsers.ParserConfigurationException
import org.jsoup.Jsoup
import de.richtercloud.selenium.tools.WaitExceptionActions
import de.richtercloud.selenium.tools.WaitExceptionAction
import org.openqa.selenium.support.ui.FluentWait
import org.openqa.selenium.support.ui.WebDriverWait
import de.richtercloud.jsf.validation.service.MessageValidatorMessageMatcher
import org.apache.commons.lang3.RandomStringUtils
import de.richtercloud.selenium.tools.ResponseCodeUnequalsException
import org.apache.commons.collections4.iterators.PermutationIterator
import org.apache.commons.io.IOUtils
import org.apache.commons.lang3.StringUtils
import org.apache.http.HttpResponse
import org.openqa.selenium.*
import org.openqa.selenium.NoSuchElementException
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.xml.sax.SAXException
import java.io.ByteArrayOutputStream
import java.lang.StringBuilder
import java.net.HttpURLConnection
import java.net.URL
import java.nio.charset.Charset
import java.nio.file.Files
import java.util.*
import java.util.function.Function

/**
 * Provides helper methods for Selenium functional tests.
 *
 * Wraps counters for screenshots and other states which require initialization.
 *
 * Passing browser at every method avoid handling of initialization of @Drone fields.
 *
 * Most methods are non-static because they involve taking screenshots on failures which involves non-static screenshot
 * counter.
 *
 * @author richter
 */
open class SeleniumHelper @JvmOverloads constructor(
    screenshotStartMarker: String,
    screeenshotEndMarker: String,
    pageSourceStartMarker: String,
    pageSourceEndMarker: String,
    webDriverWaitTimeout: Int,
    screenshotDir: File,
    validationService: ValidationService = MemoryValidationService(true)
) {
    private val screenshotDir: File
    private var screenshotCounter = 0

    /**
     * The marker added in a new line before logging the screenshot in any mode
     * which involves logging. Has no effect on screenshot file output mode. Can
     * be set to the empty string, but then an empty line is still added to the
     * output. Must never be `null`.
     */
    private val screenshotStartMarker: String

    /**
     * The marker added in a new line after logging the screenshot in any mode
     * which involves logging. See [.screenshotStartMarker] for further
     * details.
     */
    private val screenshotEndMarker: String

    /**
     * The marker added in a new line before logging the page source. See
     * [.screenshotStartMarker] for further details.
     */
    private val pageSourceStartMarker: String

    /**
     * The marker added in a new line after logging the page source. See
     * [.screenshotStartMarker] for further details.
     */
    private val pageSourceEndMarker: String

    /**
     * The time in seconds for
     * [.webDriverWait]
     * and [.webDriverWait]
     * to wait for the passed condition.
     */
    private val webDriverWaitTimeout: Int
    private val validationService: ValidationService
    /**
     * Creates a new `SeleniumHelper` using default values
     * [.SCREENSHOT_START_MARKER_DEFAULT],
     * [.SCREENSHOT_END_MARKER_DEFAULT],
     * [.PAGE_SOURCE_START_MARKER_DEFAULT],
     * [.PAGE_SOURCE_END_MARKER_DEFAULT] and a temporary screenshot
     * directory
     * @param webDriverWaitTimeout the web driver wait timeout
     * @throws IOException if an I/O exception occured during creation of
     * temporary directory for screenshot
     */
    /**
     * Creates a new `SeleniumHelper` using the defaults of
     * [.SeleniumHelper] and
     * [.WEB_DRIVER_WAIT_TIMEOUT_DEFAULT] for the web driver wait timeout
     * @throws IOException if an I/O exception occured during creation of
     * temporary directory for screenshot
     */
    @JvmOverloads
    constructor(webDriverWaitTimeout: Int = WEB_DRIVER_WAIT_TIMEOUT_DEFAULT) : this(
        SCREENSHOT_START_MARKER_DEFAULT,
        SCREENSHOT_END_MARKER_DEFAULT,
        PAGE_SOURCE_START_MARKER_DEFAULT,
        PAGE_SOURCE_END_MARKER_DEFAULT,
        webDriverWaitTimeout,
        Files.createTempDirectory("selenium-helper-screenshots").toFile()
    ) {
    }

    constructor(screenshotDir: File) : this(
        SCREENSHOT_START_MARKER_DEFAULT,
        SCREENSHOT_END_MARKER_DEFAULT,
        PAGE_SOURCE_START_MARKER_DEFAULT,
        PAGE_SOURCE_END_MARKER_DEFAULT,
        WEB_DRIVER_WAIT_TIMEOUT_DEFAULT,
        screenshotDir
    ) {
    }

    constructor(
        webDriverWaitTimeout: Int,
        screenshotDir: File
    ) : this(
        SCREENSHOT_START_MARKER_DEFAULT,
        SCREENSHOT_END_MARKER_DEFAULT,
        PAGE_SOURCE_START_MARKER_DEFAULT,
        PAGE_SOURCE_END_MARKER_DEFAULT,
        webDriverWaitTimeout,
        screenshotDir
    ) {
    }

    @Throws(WebDriverWaitException::class)
    fun retrieveMessagesText(
        browser: WebDriver,
        messages: WebElement
    ): String {
        return retrieveMessagesText(
            browser,
            messages,
            null,  //expectedText (null means no comparison to expected text)
            false //waitForExpectedText
        )
    }

    /**
     * Retrieves the text value of the nested elements inside a
     * `p:messages`.
     *
     * @param browser the web driver reference to use
     * @param messages the web element carrying the messages id whose child
     * elements will be accessed to retrieve the message
     * @param expectedText allows to optionally wait until the nested element
     * containing the text has the specified value if it's not `null`
     * @return the messages text (`expectedText` in case it's not
     * `null`
     * @throws IOException if an I/O Exception during taking a screenshot on not
     * finding expected elements occurs
     */
    @Throws(WebDriverWaitException::class)
    private fun retrieveMessagesText(
        browser: WebDriver,
        messages: WebElement,
        expectedText: String?,
        waitForExpectedText: Boolean
    ): String {
        LOGGER.trace("retrieveMessagesText expectedText: $expectedText")
        LOGGER.trace("retrieveMessagesText messages.text: ${messages.text}")
        webDriverWait(browser, ExpectedConditions.visibilityOf(messages))
        //- approaches to query the li element which contains the different
        //messages in order to increase flexibility towards checking the
        //presence for different messages has been replaced with this approach
        //based on `WebElement.getText` which is easier and allows to check for
        //different message order with permutations of the text
        //- waiting for the text to appear is unnecessary and leads to extreme
        //delay when querying a large set of text permutations because wrong
        //innerHTML is probably a phantomjs bug and the waiting doesn't work
        //around it
        if (expectedText != null) {
            if (waitForExpectedText) {
                try {
                    webDriverWait(
                        browser,
                        ExpectedConditions.textToBePresentInElement(
                            messages,
                            expectedText
                        )
                    )
                } catch (ex: TimeoutException) {
                    throw MessageTextNotContainedException(
                        expectedText,
                        messages.text
                    )
                }
            }
            if (expectedText != messages.text) {
                throw MessageTextNotContainedException(
                    expectedText,
                    messages.text
                )
                //re-using the exception internally
            }
        }
        val retValue = messages.text
        assert(expectedText == null || expectedText == retValue) {
            "expectedText '$expectedText' is not equal to retValue '$retValue'"
        }
        return retValue
    }

    /**
     * Allows to retrieve the text parameter passed to
     * [ExpectedConditions.textToBePresentInElement]
     * as well as test the type which is useful for unit testing.
     */
    inner class TextToBePresentInElementWithText protected constructor(
        private val element: WebElement,
        val text: String
    ) : ExpectedCondition<Boolean?> {
        override fun apply(driver: WebDriver?): Boolean? {
            return ExpectedConditions.textToBePresentInElement(element, text).apply(driver)
        }

        override fun toString(): String {
            return ExpectedConditions.textToBePresentInElement(element, text).toString()
        }
    }

    /**
     * Don't use JUnit 5 Assertions.fail or other methods since they confuse
     * jqwik tests
     */
    @JvmOverloads
    @Throws(WebDriverWaitException::class)
    fun assertMessagesContains(
        browser: WebDriver,
        messages: WebElement,
        messageText: String,
        waitForMessageText: Boolean =
            false //waitForMessageText
    ) {
        webDriverWait(
            browser,
            ExpectedConditions.visibilityOf(messages)
        )
        val messagesText = retrieveMessagesText(
            browser,
            messages,
            messageText,
            waitForMessageText
        )
        assert(messageText == messagesText)
    }

    /**
     * Checks whether any string in `text` is contained in the
     * `p:messages` element `messages` using
     * [.webDriverWait]
     * and returns after the first match or fails using
     * [MessageTextNotContainedException] if none of `texts` match.
     *
     * Depending on the Selenium configuration this might a TimeoutException for
     * every non-matching string in `texts` which can be ignored.
     *
     * Don't use JUnit 5 Assertions.fail or other methods since they confuse
     * jqwik tests
     *
     * @param browser the web driver to use
     * @param messages the messages element to investigate
     * @param texts the texts to check
     * @throws WebDriverWaitException if an I/O exception occurs during taking of
     * screenshots in case expected elements aren't present in `messages`
     * @throws IllegalArgumentException if `texts` is `null` or
     * empty in order to be able to detect unexpected text conditions early
     */
    @Throws(WebDriverWaitException::class)
    fun assertMessagesContainsAny(
        browser: WebDriver,
        messages: WebElement,
        texts: Set<String>
    ) {
        require(texts.isNotEmpty()) { "texts mustn't be empty" }
        //run with wait for the first element in order to catch all AJAX updates
        //and then assume that all updates have been done and thus...
        val textsItr = texts.iterator()
        var text = textsItr.next()
        val messagesText: String
        messagesText = try {
            retrieveMessagesText(
                browser,
                messages,
                text,
                true //waitForExpectedText
            )
            return
        } catch (ex: MessageTextNotContainedException) {
            retrieveMessagesText(
                browser,
                messages,
                null,
                false //waitForExpectedText
            )
        }
        //...check the rest of tests without wait
        while (textsItr.hasNext()) {
            text = textsItr.next()
            if (text == messagesText) {
                return
            }
        }
        LOGGER.trace("messages.innerHTML: ${messages.getAttribute("innerHTML")}")
        throw MessageTextNotContainedException(
            texts,
            messages.text
        )
    }

    @Throws(IOException::class)
    private fun compressGzipAndEncodeBytes(screenshotBytes: ByteArray): String {
        val byteArrayOutputStream = ByteArrayOutputStream()
        val gZIPOutputStream = GZIPOutputStream(byteArrayOutputStream)
        val screenshotBytesInputStream = ByteArrayInputStream(screenshotBytes)
        IOUtils.copy(
            screenshotBytesInputStream,
            gZIPOutputStream
        )
        val compressedScreenshotBytes = byteArrayOutputStream.toByteArray()
        return Base64.getEncoder().encodeToString(compressedScreenshotBytes)
    }
    /**
     * Takes a screenshot of `browser` and stores it according to the
     * value of the system property
     * [.SCREENSHOT_OUTPUT_MODE_PROPERTY_NAME] while managing the
     * numbering and description in file names.
     *
     * @param browser the browser to take the screenshot from
     * @param description the description to use in the file name (mustn't be
     * `null`, but is allowed to be empty because the `null` check
     * is only performed in order to guard again unwanted `null` passing)
     * @throws WebDriverWaitException if the storing method throws an I/O exception
     * @throws IllegalArgumentException if `browser` or
     * `description` is `null`
     */
    /**
     * Takes a screenshot with description [.NO_DESCRIPTION].
     *
     * @param browser the browser to take the screenshot from.
     * @throws WebDriverWaitException if the storing method throws an I/O exception
     * @see .screenshot
     */
    @JvmOverloads
    @Throws(WebDriverWaitException::class)
    fun screenshot(
        browser: WebDriver,
        description: String = NO_DESCRIPTION
    ) {
        if (browser !is TakesScreenshot) {
            LOGGER.debug("browser doesn't support taking screenshots")
            return
        }
        val screenshotOutputMode = System.getProperty(
            SCREENSHOT_OUTPUT_MODE_PROPERTY_NAME,
            SCREENSHOT_OUTPUT_MODE_FILE
        )
        when (screenshotOutputMode) {
            SCREENSHOT_OUTPUT_MODE_FILE -> {
                val scrFile = (browser as TakesScreenshot).getScreenshotAs(OutputType.FILE)
                try {
                    FileUtils.copyFile(
                        scrFile,
                        File(screenshotDir, generateScreenshotFilename(screenshotCounter, description))
                    )
                } catch (ex: IOException) {
                    throw WebDriverWaitException(ex)
                }
            }
            SCREENSHOT_OUTPUT_MODE_BASE64 -> {
                val screenshotBase64 = (browser as TakesScreenshot).getScreenshotAs(OutputType.BASE64)
                LOGGER.info(
                    "base64 encoding of screenshot with description '$description':\n" +
                            "$screenshotStartMarker\n$screenshotBase64\n$screenshotEndMarker"
                )
            }
            SCREENSHOT_OUTPUT_MODE_BASE64_GZIP -> {
                val screenshotBytes = (browser as TakesScreenshot).getScreenshotAs(OutputType.BYTES)
                val compressedScreenshotBase64: String
                compressedScreenshotBase64 = try {
                    compressGzipAndEncodeBytes(screenshotBytes)
                } catch (ex: IOException) {
                    throw WebDriverWaitException(ex)
                }
                LOGGER.info(
                    "gzip compresed base64 encoding of screenshot with description '$description':\n" +
                            "$screenshotStartMarker\n$compressedScreenshotBase64\n$screenshotEndMarker"
                )
            }
            else -> throw IllegalArgumentException("Illegal value '$screenshotOutputMode' specified for property $SCREENSHOT_OUTPUT_MODE_PROPERTY_NAME")
        }
        screenshotCounter++
    }

    @JvmOverloads
    @Throws(WebDriverWaitException::class)
    fun screenshotFullPage(
        browser: WebDriver?,
        description: String =
            NO_DESCRIPTION
    ) {
        val screenshotOutputMode = System.getProperty(
            SCREENSHOT_OUTPUT_MODE_PROPERTY_NAME,
            SCREENSHOT_OUTPUT_MODE_FILE
        )
        val fpScreenshot: Screenshot
        when (screenshotOutputMode) {
            SCREENSHOT_OUTPUT_MODE_FILE -> {
                fpScreenshot =
                    AShot().shootingStrategy(ShootingStrategies.viewportPasting(1000)).takeScreenshot(browser)
                val outputFile = File(screenshotDir, generateScreenshotFilename(screenshotCounter, description))
                LOGGER.info(
                    "screenshot output filename: {}",
                    outputFile.name
                )
                try {
                    ImageIO.write(fpScreenshot.image, "PNG", outputFile)
                } catch (ex: IOException) {
                    throw WebDriverWaitException(ex)
                }
            }
            SCREENSHOT_OUTPUT_MODE_BASE64 -> {
                fpScreenshot =
                    AShot().shootingStrategy(ShootingStrategies.viewportPasting(1000)).takeScreenshot(browser)
                val imageOutputStream = ByteArrayOutputStream()
                try {
                    ImageIO.write(fpScreenshot.image, "PNG", imageOutputStream)
                } catch (ex: IOException) {
                    throw WebDriverWaitException(ex)
                }
                val screenshotBase64 = Base64.getEncoder().encodeToString(imageOutputStream.toByteArray())
                LOGGER.info("base64 encoding of screenshot with description '$description':\n$screenshotStartMarker\n$screenshotBase64\n$screenshotEndMarker")
            }
            SCREENSHOT_OUTPUT_MODE_BASE64_GZIP -> {
                fpScreenshot =
                    AShot().shootingStrategy(ShootingStrategies.viewportPasting(1000)).takeScreenshot(browser)
                val imageOutputStream = ByteArrayOutputStream()
                try {
                    ImageIO.write(fpScreenshot.image, "PNG", imageOutputStream)
                    val compressedScreenshotBase64 = compressGzipAndEncodeBytes(imageOutputStream.toByteArray())
                    LOGGER.info(
                        "gzip compresed base64 encoding of screenshot with description '$description':\n" +
                                "$screenshotStartMarker\n$compressedScreenshotBase64\n$screenshotEndMarker"
                    )
                } catch (ex: IOException) {
                    throw WebDriverWaitException(ex)
                }
            }
            else -> throw IllegalArgumentException("Illegal value '$screenshotOutputMode' specified for property $SCREENSHOT_OUTPUT_MODE_PROPERTY_NAME")
        }
        screenshotCounter++
    }

    private fun generateScreenshotFilename(
        screenshotCounter: Int,
        description: String
    ): String {
        return String.format("%05d-%s.png", screenshotCounter, description)
    }

    @JvmOverloads
    @Throws(SAXException::class, IOException::class, PageSourceInvalidException::class)
    fun validatePageSource(
        pageSource: String,
        minimumLevel: ValidatorMessageSeverity = ValidatorMessageSeverity.WARNING,
        ignore: Matcher<ValidatorMessage> = MISSING_DOCTYPE_MESSAGE_MATCHER
    ) {
        val violations = validationService.validateMessages(
            ByteArrayInputStream(pageSource.toByteArray()),
            minimumLevel,
            ignore
        )
        if (!violations.isEmpty()) {
            throw PageSourceInvalidException(ValidationService.transformValidatorResponse(violations))
        }
    }

    fun logPageSource(
        browser: WebDriver,
        logger: Logger
    ) {
        logPageSource(
            LOG_DESCRIPTION_PAGE_SOURCE,
            browser.pageSource,
            logger
        )
    }

    /**
     * Logs the page source retrieved from `browser` to `logger` as
     * info message.
     *
     * @param browser the web driver to get the page source from
     * @param logger the logger to send the info message to
     * @param validationMinimumLevel only validation message with this level can
     * cause the page source to be considered invalid
     * @param ignore allows to specify specific messages which ought to be
     * ignored or criteria to ignore a number of messages
     * @throws WebDriverWaitException wraps exceptions during logging, validation and setup of validation
     */
    @JvmOverloads
    @Throws(WebDriverWaitException::class)
    fun logPageSourcePretty(
        browser: WebDriver,
        logger: Logger,
        validationMinimumLevel: ValidatorMessageSeverity = ValidatorMessageSeverity.WARNING,
        ignore: Matcher<ValidatorMessage> = nothing
    ) {
        val pageSource = browser.pageSource
        try {
            validatePageSource(
                pageSource,
                validationMinimumLevel,
                ignore
            )
            //always validate since there's no sense in passing invalid XHTML to
            //the pretty printer
        } catch (ex: SAXException) {
            throw WebDriverWaitException(ex)
        } catch (ex: IOException) {
            throw WebDriverWaitException(ex)
        } catch (ex: PageSourceInvalidException) {
            throw WebDriverWaitException(ex)
        }
        val pageSourcePretty = prettyPrintPageSource(pageSource)
        logPageSource(
            LOG_DESCRIPTION_PAGE_SOURCE,
            pageSourcePretty,
            logger
        )
    }

    @Throws(IOException::class, SAXException::class, PageSourceInvalidException::class)
    fun logPageSourceURLPretty(
        url: URL,
        logger: Logger,
        validationMinimumLevel: ValidatorMessageSeverity,
        ignore: Matcher<ValidatorMessage>
    ) {
        val pageSource = IOUtils.toString(
            url,
            Charset.defaultCharset()
        )
        validatePageSource(
            pageSource,
            validationMinimumLevel,
            ignore
        )
        //always validate since there's no sense in passing invalid XHTML to
        //the pretty printer
        val pageSourcePretty = prettyPrintPageSource(pageSource)
        logPageSource(
            LOG_DESCRIPTION_PAGE_SOURCE,
            pageSourcePretty,
            logger
        )
    }

    private fun logPageSource(
        description: String,
        pageSource: String,
        logger: Logger
    ) = logger.info("$description:\n$pageSourceStartMarker\n$pageSource\n$pageSourceEndMarker")

    /**
     * Logs the current state of the DOM tree by executing
     * `return document.childNodes[1].outerHTML;` in `browser`.
     *
     * @param browser the web driver to use
     * @param logger the target logger
     * @throws javax.xml.parsers.ParserConfigurationException if a parser
     * configuration error occurs during setup of pretty printing
     * dependencies
     * @throws org.xml.sax.SAXException if a SAX exception occurs during setup
     * of pretty printing dependencies
     * @throws IOException if an I/O error occurs
     */
    @Throws(ParserConfigurationException::class, SAXException::class, IOException::class)
    fun logDOMTree(
        browser: JavascriptExecutor,
        logger: Logger
    ) {
        val htmlOuterHtml = browser.executeScript("return document.childNodes[1].outerHTML;") as String
        //asked
        //https://stackoverflow.com/questions/49692420/how-to-get-a-org-w3c-doc-document-or-node-reference-with-selenium-in-java
        //for how to obtain a org.w3c.dom.Document or Node reference
        //directly
        val pageSourcePretty = prettyPrintPageSource(htmlOuterHtml)
        logPageSource(
            LOG_DESCRIPTION_DOM_TREE,
            pageSourcePretty,
            logger
        )
    }

    private fun prettyPrintPageSource(pageSource: String): String {
        //Pretty printer choice:
        //- JTidy is unmaintained and hard to configure to do nothing else but
        //pretty printing
        //- `LSSerializer` together with
        //`com.sun.org.apache.xerces.internal.dom.DOMImplementationSourceImpl`
        //(from
        //http://www.java2s.com/Tutorials/Java/XML_HTML_How_to/DOM/Pretty_print_XML.htm
        //) causes
        //`org.xml.sax.SAXParseException; lineNumber: 2; columnNumber: 39; The element type "link" must be terminated by the matching end-tag "</link>".`
        //for valid XHTML
        val doc = Jsoup.parse(pageSource)
        doc.outputSettings().prettyPrint(true)
        val pageSourcePretty = doc.html()
        //- `LSSerializer` changes `<head></head>` to `<head/>`, but no
        //manipuation except whitespace ought to take place, asked
        //https://stackoverflow.com/questions/49555441/how-to-avoid-html-being-manipulated-during-pretty-printing-with-lsserializer
        //for input
        if (StringUtils.startsWithIgnoreCase(pageSource, DOCTYPE_TEMPLATE)) {
            assert(StringUtils.startsWithIgnoreCase(pageSource, DOCTYPE_TEMPLATE))
            assert(pageSource.contains("<html")) {
                "pageSource doesn't contain '<html': $pageSource"
            }
            (pageSource.substring(pageSource.indexOf("<html")).replace(WHITESPACE_TEMPLATE.toRegex(), "")
                    == pageSourcePretty.substring(pageSourcePretty.indexOf("<html"))
                .replace(WHITESPACE_TEMPLATE.toRegex(), ""))
        } else {
            (pageSource.replace(WHITESPACE_TEMPLATE.toRegex(), "")
                    == pageSourcePretty.replace(WHITESPACE_TEMPLATE.toRegex(), ""))
        }
        return pageSourcePretty
    }

    @Throws(WebDriverWaitException::class)
    fun <V> webDriverWait(
        browser: WebDriver,
        condition: Function<in WebDriver?, V>
    ) {
        webDriverWait(
            browser,
            condition,
            NO_DESCRIPTION
        )
    }

    @Throws(WebDriverWaitException::class)
    fun <V> webDriverWait(
        browser: WebDriver,
        condition: Function<in WebDriver?, V>,
        exceptionScreenshotDescription: String
    ) {
        webDriverWait(
            browser,
            condition,
            exceptionScreenshotDescription,
            webDriverWaitTimeout
        )
    }

    /**
     * Creates a [WebDriverWait] and allows the timeout configuration in
     * this instance to be overridden (while other configuration parameters of
     * this instance can still be used).
     * @param <V> the type of the function
     * @param browser the web driver to use
     * @param condition the condition to wait for
     * @param exceptionScreenshotDescription the timeout screenshot description
     * @param overrideWebDriverWaitTimout the wait timeout
     * @throws WebDriverWaitException in case taking the failure screenshot causes such an
     * exception
    </V> */
    @Throws(WebDriverWaitException::class)  //suppress PMD.UnnecessaryFullyQualifiedName until https://github.com/pmd/pmd/issues/2029 is fixed
    fun <V> webDriverWait(
        browser: WebDriver,
        condition: Function<in WebDriver?, V>,
        exceptionScreenshotDescription: String,
        overrideWebDriverWaitTimout: Int
    ) {
        webDriverWait(
            browser,
            condition,
            exceptionScreenshotDescription,
            overrideWebDriverWaitTimout,
            WaitExceptionActions.screenshot()
        )
    }

    /**
     * Creates a [WebDriverWait] and allows the timeout configuration in
     * this instance to be overridden (while other configuration parameters of
     * this instance can still be used).
     * @param <V> the type of the function
     * @param browser the web driver to use
     * @param condition the condition to wait for
     * @param exceptionDescription the timeout screenshot description
     * @param waitExceptionActions the actions to take in case of an exception during wait
     * @throws WebDriverWaitException in case taking the failure screenshot causes such an
     * exception
    </V> */
    @Throws(WebDriverWaitException::class)
    fun <V> webDriverWait(
        browser: WebDriver,
        condition: Function<in WebDriver?, V>,
        exceptionDescription: String,
        vararg waitExceptionActions: WaitExceptionAction
    ) {
        webDriverWait(
            browser,
            condition,
            exceptionDescription,
            webDriverWaitTimeout,
            *waitExceptionActions
        )
    }

    /**
     * Creates a [WebDriverWait] and allows the timeout configuration in
     * this instance to be overridden (while other configuration parameters of
     * this instance can still be used).
     * @param <V> the type of the function
     * @param browser the web driver to use
     * @param condition the condition to wait for
     * @param exceptionDescription the timeout screenshot description
     * @param overrideWebDriverWaitTimout the wait timeout
     * @param waitExceptionActions the actions to take in case of an exception during wait
     * @throws WebDriverWaitException in case taking the failure screenshot causes such an
     * exception
    </V> */
    @Throws(WebDriverWaitException::class)
    fun <V> webDriverWait(
        browser: WebDriver,
        condition: Function<in WebDriver?, V>,
        exceptionDescription: String,
        overrideWebDriverWaitTimout: Int,
        vararg waitExceptionActions: WaitExceptionAction
    ) {
        try {
            createWebDriverWait(
                browser,
                overrideWebDriverWaitTimout.toLong()
            ).until(condition)
        } catch (ex: TimeoutException) {
            LOGGER.trace(
                "WebDriverWait timed out, performing actions $waitExceptionActions, see nested "
                        + "exception for details",
                ex
            )
            handleWaitException(
                browser, "web driver wait timeout exception for element '$exceptionDescription'",
                *waitExceptionActions
            )
            throw ex
        } catch (ex: NoSuchElementException) {
            LOGGER.trace(
                "NoSuchElementException occured, made screenshot, see "
                        + "nested exception for details",
                ex
            )
            handleWaitException(
                browser,
                "web driver wait no-such-element exception for element '$exceptionDescription'",
                *waitExceptionActions
            )
            throw ex
        }
    }

    @Throws(WebDriverWaitException::class)
    fun handleWaitException(
        browser: WebDriver,
        description: String,
        vararg waitExceptionActions: WaitExceptionAction
    ) {
        for (waitExceptionAction in waitExceptionActions) {
            waitExceptionAction.perform(this, browser, description)
        }
    }

    /**
     * Factory method for tests which has to be used until support for mocking
     * constructors via instrumentalisation with PowerMockito works in JUnit 5
     * (see comments in SeleniumHelperTest.testAssertMessagesContainsAny for
     * details and an issue report link).
     *
     * @param webDriver the web driver to use
     * @return a new `FluentWait` reference
     */
    protected open fun createFluentWait(webDriver: WebDriver): FluentWait<WebDriver> {
        return FluentWait(webDriver)
    }

    protected open fun createWebDriverWait(
        webDriver: WebDriver,
        timeOutInSeconds: Long
    ): WebDriverWait {
        return WebDriverWait(
            webDriver,
            timeOutInSeconds
        )
    }

    companion object {
        private val LOGGER = LoggerFactory.getLogger(SeleniumHelper::class.java)
        private const val SCREENSHOT_OUTPUT_MODE_PROPERTY_NAME = "screenshotOutputMode"
        private const val SCREENSHOT_OUTPUT_MODE_FILE = "file"
        private const val SCREENSHOT_OUTPUT_MODE_BASE64 = "base64"

        /**
         * Compresses images in gzip format and provides the base64-encoded result
         * of the encryption in order to reduce log size on continuous integration
         * services.
         */
        private const val SCREENSHOT_OUTPUT_MODE_BASE64_GZIP = "base64-gzip"
        private const val NO_DESCRIPTION = "no description"
        private const val DOCTYPE_TEMPLATE = "<!DOCTYPE html>"
        private const val WHITESPACE_TEMPLATE = "\\s+"
        const val SCREENSHOT_START_MARKER_DEFAULT = "---"
        const val SCREENSHOT_END_MARKER_DEFAULT = "---"
        const val PAGE_SOURCE_START_MARKER_DEFAULT = "==="
        const val PAGE_SOURCE_END_MARKER_DEFAULT = "==="
        const val WEB_DRIVER_WAIT_TIMEOUT_DEFAULT = 5
        private const val LOG_DESCRIPTION_PAGE_SOURCE = "browser page source"
        private const val LOG_DESCRIPTION_DOM_TREE = "DOM tree"

        /**
         * Accounts for the fact that some Selenium driver implementations don't include the doctype in their page source
         * return value, but we might want to evaluate the driver's page source rather than the real page's source.
         */
        val MISSING_DOCTYPE_MESSAGE_MATCHER: Matcher<ValidatorMessage> = MessageValidatorMessageMatcher(
            "Start tag seen without seeing a doctype first. Expected “<!DOCTYPE html>”."
        )

        /**
         * Wrapper around random string generation which allows to quickly avoid
         * temporary problems like Chrome Selenium driver not accepting unicode
         * characters outside Basic Multilingual Plane (BMP) (asked
         * https://stackoverflow.com/questions/46950897/how-to-produce-random-basic-multilingual-plane-bmp-strings-in-java
         * for inputs)
         *
         * @param length the length of the produced string
         * @return the produced random string
         */
        @JvmStatic
        fun randomString(length: Int): String {
            require(length > 0) { "length mustn't be <= 0" }
            return RandomStringUtils.randomAlphanumeric(length)
        }

        fun retrievePlainText(browser: WebDriver): String {
            val preElement = browser.findElement(By.xpath("/html/body/pre"))
            return preElement.text
        }

        /**
         * Creates the permutation of `texts` which might be useful in
         * [.assertMessagesContainsAny]
         * in order to reflect that validation constraint violoations are generally
         * in undefined order since `Validator.validate` returns a
         * `Set`.This methods creates `n` faculty (`n!`) items for `n`
         * items in `text` which can take a long time to check (five items can
         * already take up to a minute).
         *
         * Note that the type of the collection used for `texts` determines
         * whether the return value contains the item more than once or not.
         *
         * An example for steam fetichist (involving SteamEx stream extension):
         * `createMessagePermutation(StreamEx.of(entities.stream())
         * .map(prop -> String.format("invalid: key '%s' mustn't be mapped to null",
         * prop.getName()))
         * .append("invalid: mapping for this entity isn't valid. Details should have been given to you.")
         * .collect(Collectors.toSet()))
         * .stream()
         * .map(perm -> StreamEx.of(perm)
         * .joining("\n",
         * String.format("%s\n"
         * + "The following constraints are violated:\n",
         * BackingBean.X_FAILED_ERROR_SUMMARY),
         * String.format("\nFix the corresponding values in the components.")))
         * //joining isn't necessary because \n could have been
         * //added to each of the permutation items, this was
         * //used as a way to get to know the awesome StreamEx
         * //library
         * .collect(Collectors.toSet());
        ` *
         *
         * @param texts the texts to permutate
         * @return the created permutation
         */
        fun createMessagePermutation(texts: Collection<String>?): Set<List<String>> {
            val retValue: MutableSet<List<String>> = HashSet()
            val textsPermutationIterator = PermutationIterator(texts)
            textsPermutationIterator.forEachRemaining({ perm: List<String> -> retValue.add(perm) })
            return retValue
        }

        fun createMessagePermutation(vararg texts: String?): Set<List<String>> {
            return createMessagePermutation(HashSet(Arrays.asList(*texts)))
        }

        /**
         * Compares the response code of `uRLConnection` to
         * `responseCode`
         *
         *  Don't use JUnit 5 Assertions.fail or other methods since they confuse
         * jqwik tests
         *
         * @param responseCode the expected response code
         * @param httpResponse the response to check
         * @throws IOException if an I/O Exception happens during constructions of
         * the exception message
         * @throws ResponseCodeUnequalsException if the response code isn't the
         * expected `responseCode`
         */
        @Throws(IOException::class)
        fun assertResponseCodeEquals(
            responseCode: Int,
            httpResponse: HttpResponse
        ) {
            if (responseCode != httpResponse.statusLine.statusCode) {
                throw ResponseCodeUnequalsException(httpResponse)
            }
        }

        /**
         * Compares the response code of `uRLConnection` to
         * `responseCode`
         *
         *  Don't use JUnit 5 Assertions.fail or other methods since they confuse
         * jqwik tests
         *
         * @param responseCode the expected response code
         * @param uRLConnection the connection to check
         * @throws IOException if an I/O Exception happens during constructions of
         * the exception message
         * @throws ResponseCodeUnequalsException if the response code isn't the
         * expected `responseCode`
         */
        @Throws(IOException::class)
        fun assertResponseCodeEquals(
            responseCode: Int,
            uRLConnection: HttpURLConnection
        ) {
            if (responseCode != uRLConnection.responseCode) {
                throw ResponseCodeUnequalsException(uRLConnection)
            }
        }

        /**
         * Retrieves a specific menu item in the nested submenus opened based on the
         * specified indices assuming that all menu items are links.
         * @param slideMenu the slide menu element
         * @param indices the indices of submenus to open
         * @return the found web element
         */
        fun retrieveSlideMenuItem(
            slideMenu: WebElement,
            vararg indices: Int
        ): WebElement {
            val xPath = buildXPathForSlideMenuIndices(*indices)
            return slideMenu.findElement(By.xpath(xPath))
        }

        /**
         * Retrieves all menu items in the nested submenus opened based on the
         * specified indices assuming that all menu items are links.
         * @param slideMenu the slide menu element
         * @param indices the indices of submenus to open
         * @return the found web elements
         */
        fun retrieveSlideMenuItems(
            slideMenu: WebElement,
            vararg indices: Int
        ): List<WebElement> {
            val xPath = buildXPathForSlideMenuIndices(*indices)
            return slideMenu.findElements(By.xpath(xPath))
        }

        private fun buildXPathForSlideMenuIndices(vararg indices: Int): String {
            val xPathBuilder = StringBuilder(128)
            xPathBuilder.append("div/div[1]/")
            for (index in indices) {
                xPathBuilder.append("ul/li[$index]/")
            }
            xPathBuilder.append('a')
            val xPath = xPathBuilder.toString()
            LOGGER.trace(
                "buildXPathForSlideMenuIndices xPath: {}",
                xPath
            )
            return xPath
        }

        /**
         * Allows to check for the absence of a web element. See
         * https://stackoverflow.com/questions/6533597/webdriver-how-to-check-if-an-page-object-web-element-exists
         * which is referencing
         * https://groups.google.com/forum/#!topic/webdriver/kJqbbLVo40E for an
         * explanation why this is a good if not the only way to perform the check.
         * @param webElement the web element whose absence to check
         * @return the created condition
         */
        fun absenceOfWebElement(webElement: WebElement): ExpectedCondition<Boolean> {
            return ExpectedCondition { f: WebDriver? ->
                try {
                    webElement.isDisplayed()
                    false
                } catch (ex: NoSuchElementException) {
                    true
                }
            }
        }
    }

    /**
     * Creates a new `SeleniumHelper`.
     * @param screenshotStartMarker the start marker for logging text-encoded
     * screenshots
     * @param screeenshotEndMarker the end marker for logging text-encoded
     * screenshots
     * @param pageSourceStartMarker the start marker to make finding page logs
     * easier
     * @param pageSourceEndMarker the end marker to make finding page logs
     * easier
     * @param webDriverWaitTimeout the web driver wait timeout
     * @param screenshotDir the directory where to store screenshots (will be
     * created if it doesn't exist and has to be an empty directory in case the
     * path exists)
     * @throws IOException if an I/O exception occurs during the optional
     * creation of `screenshotDir`
     * @throws IllegalArgumentException if `screenshotStartMarker`,
     * `screeenshotEndMarker`, `pageSourceStartMarker`,
     * `pageSourceEndMarker` or `screenshotDir` are `null`
     */
    init {
        this.screenshotStartMarker = screenshotStartMarker
        this.screenshotEndMarker = screeenshotEndMarker
        this.pageSourceStartMarker = pageSourceStartMarker
        this.pageSourceEndMarker = pageSourceEndMarker
        if (!screenshotDir.exists()) {
            Files.createDirectories(screenshotDir.toPath())
        } else if (screenshotDir.exists()) {
            if (!screenshotDir.isDirectory) {
                throw IllegalArgumentException(
                    "screenshot directory '${screenshotDir.absolutePath}' points to an " +
                            "existing path which is not a directory"
                )
            } else if (screenshotDir.list().size != 0) {
                throw IllegalArgumentException(
                    "screenshot directory '${screenshotDir.absolutePath}' points to an " +
                            "existing directory which is not empty"
                )
            }
        }
        this.screenshotDir = screenshotDir
        LOGGER.info("screenshot directory is '${screenshotDir.absolutePath}'")
        this.webDriverWaitTimeout = webDriverWaitTimeout
        this.validationService = validationService
    }
}