/**
 * Copyright 2018-2021 Karl-Philipp Richter
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package de.richtercloud.selenium.tools

import org.openqa.selenium.WebDriver
import org.slf4j.Logger

object WaitExceptionActions {

    fun screenshot(): WaitExceptionAction {
        return WaitExceptionAction { seleniumHelper: SeleniumHelper, webDriver: WebDriver, description: String ->
            seleniumHelper.screenshot(
                webDriver,
                description
            )
        }
    }

    fun screenshotFullPage(): WaitExceptionAction {
        return WaitExceptionAction { seleniumHelper: SeleniumHelper, webDriver: WebDriver, description: String ->
            seleniumHelper.screenshotFullPage(
                webDriver,
                description
            )
        }
    }

    fun logPageSource(logger: Logger): WaitExceptionAction {
        return WaitExceptionAction { seleniumHelper: SeleniumHelper, webDriver: WebDriver, description: String ->
            seleniumHelper.logPageSource(
                webDriver, logger
            )
        }
    }

    fun logPageSourcePretty(logger: Logger): WaitExceptionAction {
        return WaitExceptionAction { seleniumHelper: SeleniumHelper, webDriver: WebDriver, description: String ->
            seleniumHelper.logPageSourcePretty(
                webDriver, logger
            )
        }
    }
}